public class Singer extends Person{
    public String genre;
    public int jumlahLagu;

    public Singer(String genre, int jumlahLagu) {
        this.genre = genre;
        this.jumlahLagu = jumlahLagu;
    }

    public Singer(Integer id, String name, String gender, String address, String genre, int jumlahLagu) {
        super(id, name, gender, address);
        this.genre = genre;
        this.jumlahLagu = jumlahLagu;
    }
}
