package interfaceClass;

public class MainInterface {
    public static void main(String[] args) {
        HasBrand brand = new HasBrandImpl("Indo Copy", 50000);
        System.out.println("Brand Name "+ brand.getBrand());
        System.out.println("Price "+ brand.getPrice());

        HasBrandImpl brand1 = new HasBrandImpl("Toyota", 1000000);
        System.out.println("Brand Name "+ brand1.getBrand());
        System.out.println("Price "+ brand1.getPrice());
    }
}
