public class Student extends Person {
    public String major;
    public Integer studentId;

    public Student(String major, Integer studentId) {
        this.major = major;
        this.studentId = studentId;
    }

    public Student(Integer id, String name, String gender, String address, String major, Integer studentId) {
        super(id, name, gender, address);
        this.major = major;
        this.studentId = studentId;
    }

    public Student(Integer id, String name, String gender, String address, int age, String major, Integer studentId) {
        super(id, name, gender, address, age);
        this.major = major;
        this.studentId = studentId;
    }

    public void sayHello() {
        System.out.println("Hello I am Student");
        System.out.println("My name is " + this.name);

    }

    @Override
    public String toString() {
        return "Student{" +
                "major='" + major + '\'' +
                ", studentId=" + studentId +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", address='" + address + '\'' +
                ", age=" + age +
                '}';
    }
}