package encapsulation;

public class MainCategory {
    public static void main(String[] args) {
        Category category1 = new Category();
        category1.setId(1);
        category1.setName("Makanan");
        category1.setDesc("Junk Food");
        System.out.println(category1);

        System.out.println("");
        Category category2 = new Category(2, "Accessoris", "Perhiasan Dunia");
        System.out.println(category2);

        System.out.println("");
        System.out.println(new Category(3, "Clothes", "Baju Imut Anak-Anak"));
    }
}
