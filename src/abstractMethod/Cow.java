package abstractMethod;

public class Cow extends Animal{
    public Cow(String nama){
        this.name=nama;
    }
    @Override
    void canRun() {
        System.out.println("The cow "+name+" can run");
    }

    @Override
    void canEat() {
        System.out.println("The cow "+name+" can eat leaves");
    }

    @Override
    void canJump() {
        System.out.println("The cow "+name+" can short jump");
    }

    @Override
    void canSwim() {
        System.out.println("The cow "+name+" can not swim");
    }

    @Override
    void canClimb() {
        System.out.println("The cow "+name+" can not climb");
    }
}
