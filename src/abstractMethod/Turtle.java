package abstractMethod;

public class Turtle extends Animal{
    public Turtle(String name){
        this.name = name;
    }
    @Override
    void canRun() {
        System.out.println("The turtle "+ name+ " can not run");
    }

    @Override
    void canEat() {
        System.out.println("The turtle "+ name+" can eat vegetables");
    }

    @Override
    void canJump() {
        System.out.println("The turtle "+ name+ " can not jump");
    }

    @Override
    void canSwim() {
        System.out.println("The turtle "+ name+ " can swim");
    }

    @Override
    void canClimb() {
        System.out.println("The turtle "+ name+ " can short climb");
    }

}
