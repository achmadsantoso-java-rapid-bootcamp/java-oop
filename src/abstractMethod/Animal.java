package abstractMethod;

public abstract class Animal {
    String name;
    abstract void canRun();
    abstract void canEat();
    abstract void canJump();
    abstract void canSwim();
    abstract void canClimb();

}
