package abstractMethod;

public class Rabbit extends Animal {
    public Rabbit(String name){
        this.name= name;
    }
    @Override
    void canRun() {
        System.out.println("The rabbit "+ name+ " can not run");
    }

    @Override
    void canEat() {
        System.out.println("The rabbit "+name+" can eat carrot");
    }

    @Override
    void canJump() {
        System.out.println("The rabbit "+name+" can jump");
    }

    @Override
    void canSwim() {
        System.out.println("The rabbit "+name+" can not swim");
    }

    @Override
    void canClimb() {
        System.out.println("The rabbit "+name+" can not climb");
    }
}
