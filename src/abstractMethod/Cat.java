package abstractMethod;

public class Cat extends Animal{
    public Cat(String name){
        this.name = name;
    }


    @Override
    void canRun() {
        System.out.println("The Cat "+ name + " can run");
    }

    @Override
    void canEat() {
        System.out.println("The cat "+name+" can eat fish");
    }

    @Override
    void canJump() {
        System.out.println("The cat "+name+" can jump");
    }

    @Override
    void canSwim() {
        System.out.println("The cat "+name+" can not swim");
    }

    @Override
    void canClimb() {
        System.out.println("The cat "+name+" can climb");
    }
}
