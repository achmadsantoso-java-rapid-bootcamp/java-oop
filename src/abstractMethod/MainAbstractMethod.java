package abstractMethod;

public class MainAbstractMethod {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Citty");
        cat1.canEat();
        cat1.canRun();
        cat1.canClimb();
        cat1.canJump();
        cat1.canSwim();

        System.out.println("\n\n");
        Cow cow1 = new Cow("Dedi");
        cow1.canEat();
        cow1.canClimb();
        cow1.canJump();
        cow1.canSwim();
        cow1.canClimb();

        System.out.println("\n\n");
        Horse horse1 = new Horse("James");
        horse1.canClimb();
        horse1.canEat();
        horse1.canJump();
        horse1.canSwim();
        horse1.canRun();

        System.out.println("\n\n");
        Rabbit rabit1 = new Rabbit("Kiki");
        rabit1.canEat();
        rabit1.canClimb();
        rabit1.canJump();
        rabit1.canSwim();
        rabit1.canRun();

        System.out.println("\n\n");
        Turtle turtle1 = new Turtle("Budi");
        turtle1.canEat();
        turtle1.canClimb();
        turtle1.canJump();
        turtle1.canSwim();
        turtle1.canRun();

    }
}
