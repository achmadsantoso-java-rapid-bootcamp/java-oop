package abstractMethod;

public class Horse extends Animal{
    public Horse(String name){
        this.name = name;
    }
    @Override
    void canRun() {
        System.out.println("The horse "+ name + " can run");
    }

    @Override
    void canEat() {
        System.out.println("The horse "+ name + " can eat leaves");
    }

    @Override
    void canJump() {
        System.out.println("The horse "+ name + " can jump");
    }

    @Override
    void canSwim() {
        System.out.println("The horse "+ name + " can not swim");
    }

    @Override
    void canClimb() {
        System.out.println("The horse "+ name + " can not climb");
    }
}
