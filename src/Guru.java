public class Guru extends Person{
    public int salary;
    public String pelajaran;

    public Guru(Integer id, String name, String gender, String address, int age, int salary, String pelajaran) {
        super(id, name, gender, address, age);
        this.salary = salary;
        this.pelajaran = pelajaran;
    }
}
