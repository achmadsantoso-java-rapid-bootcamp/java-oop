public class President extends Person{
    public String partai;

    public President(Integer id, String name, String gender, String address, int age, String partai) {
        super(id, name, gender, address, age);
        this.partai = partai;
    }
}
