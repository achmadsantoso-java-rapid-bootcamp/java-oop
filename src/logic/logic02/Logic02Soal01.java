package logic.logic02;

import logic.BasicLogic;

public class Logic02Soal01 extends BasicLogic {
    public Logic02Soal01(int n) {
        super(n);
    }

    public void isiArray(){
        int angka = 1;
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.n; j++) {
                if(i==j){
                    this.array[i][j] = String.valueOf(angka);
                }else if(i+j==n-1){
                    this.array[j][i] = String.valueOf(angka);
                }
            }
            angka++;
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.print();
    }
}
