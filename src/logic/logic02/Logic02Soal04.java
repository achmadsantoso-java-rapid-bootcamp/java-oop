package logic.logic02;

import logic.BasicLogic;

public class Logic02Soal04 extends BasicLogic {
    public Logic02Soal04(int n) {
        super(n);
    }

    public void isiArray(){
        int nilaiTengah = this.n/2;

        int[][] hasil = new int[this.n][this.n];
        int[] angka = new int[this.n];
        int a;
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.n; j++) {
                if (j<=1) {
                    angka[j]=1;
                }else{
                    angka[j] = angka[j-1] + angka[j-2];
                }

                if (i == 0 || j==0 || i == n - 1 || j == n - 1 || i==nilaiTengah || j==nilaiTengah) {
                    this.array[i][j] = String.valueOf(angka[j]);
                }
            }

        }
    }

    public void cetakArray(){
        this.isiArray();
        this.print();
    }
}
