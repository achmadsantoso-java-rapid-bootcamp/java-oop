package logic.logic01;

import logic.BasicLogic;

public class Logic01Soal07 extends BasicLogic {
    public Logic01Soal07(int n) {
        super(n);
    }

    public void isiArray(){
        int cek = 0;
        for (char i = 'A'; i < 'Z'; i++) {
            if(cek==this.n){
                break;
            }
            this.array[0][cek] = String.valueOf(i);
            cek++;
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }

}
