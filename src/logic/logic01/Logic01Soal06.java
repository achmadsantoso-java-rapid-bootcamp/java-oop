package logic.logic01;

import logic.BasicLogic;

public class Logic01Soal06 extends BasicLogic {
    public Logic01Soal06(int n) {
        super(n);
    }

    public void isiArray(){
        int bil;
        int cek;
        int a = 0;
        for (int i = 0; i < 100; i++) {
            cek = 0;
            bil = 0;
            for (int j = 1; j <= i; j++) {
                if(i%j==0){
                    bil++;
                    cek++;
                }
            }
            if(a==9){
                break;
            }
            if(cek ==2 ){
                a++;
            }
            if(bil==2){
                this.array[0][a-1] = String.valueOf(i);
            }
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }

}
