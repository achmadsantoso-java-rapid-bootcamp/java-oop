package logic.logic01;

import logic.BasicLogic;

public class Logic01Soal02 extends BasicLogic {
    public Logic01Soal02(int n) {
        super(n);
    }

    public void isiArray(){
        int angka1 = 1;
        int angka2 = 3;
        for (int i = 0; i < this.n; i++) {
            if(i==0){
                this.array[0][i] = String.valueOf(angka1);
            } else if (i == 1) {
                this.array[0][i] = String.valueOf(angka2);
            } else if(i%2==0){
                angka1++;
                this.array[0][i] = String.valueOf(angka1);
            } else if(i%2==1){
                angka2+=3;
                this.array[0][i] = String.valueOf(angka2);
            }
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }

}
