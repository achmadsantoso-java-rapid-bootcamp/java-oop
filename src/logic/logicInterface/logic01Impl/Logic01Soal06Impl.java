package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal06Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal06Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int bil ;
        int cek ;
        int a = 0;
        for (int i = 0; i < 100; i++) {
            cek = 0;
            bil = 0;
            for (int j = 1; j <= i; j++) {
                if(i%j==0){
                    bil++;
                    cek++;
                }
            }
            if(a==this.logic.n){
                break;
            }
            if(bil==2){
                this.logic.array[0][a] = String.valueOf(i);
            }if(cek==2){
                a++;
            }
        }
    }
    @Override
    public void cetakArray(){
        this.isiArray();
        this.logic.printSingle();
    }
}
