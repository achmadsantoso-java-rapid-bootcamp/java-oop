package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal08Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal08Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int angka = 2;
        char huruf = 'A';
        for (int i = 0; i < this.logic.n; i++) {
            if(i%2==0){
                this.logic.array[0][i] = String.valueOf(huruf);
                huruf+=2;
            }else if(i%2==1){
                this.logic.array[0][i] = String.valueOf(angka);
                angka+=2;
            }

        }
    }
    @Override
    public void cetakArray(){
        this.isiArray();
        this.logic.printSingle();
    }
}
