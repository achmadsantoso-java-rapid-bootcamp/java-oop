package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal04Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal04Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int[][] temp = new int[this.logic.n][this.logic.n];
        for (int i = 0; i < this.logic.n; i++) {
            if(i<=1){
                this.logic.array[0][i] = String.valueOf(1);
            }else {
                temp[0][i]= Integer.parseInt(this.logic.array[0][i-2]) + Integer.parseInt(this.logic.array[0][i-1]);
                this.logic.array[0][i] = String.valueOf(temp[0][i]);
            }
        }
    }
    @Override
    public void cetakArray(){
        this.isiArray();
        this.logic.printSingle();
    }
}
