package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal02Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal02Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int angka = 1;
        int angka2 = 3;
        for (int i = 0; i < this.logic.n; i++) {
            if(i%2==0){
                this.logic.array[0][i] = String.valueOf(angka);
                angka=angka+1;
            }else if(i%2==1){
                this.logic.array[0][i] = String.valueOf(angka2);
                angka2=angka2+3;
            }
        }
    }
    @Override
    public void cetakArray(){
        this.isiArray();
        this.logic.printSingle();
    }
}
