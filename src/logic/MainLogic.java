package logic;

import logic.logic01.*;
import logic.logic02.Logic02Soal01;
import logic.logic02.Logic02Soal02;
import logic.logic02.Logic02Soal03;
import logic.logic02.Logic02Soal04;

public class MainLogic {
    public static void main(String[] args) {
        System.out.println("Logic 01");
        System.out.println("Soal No 1");
        Logic01Soal01 logic01Soal01 = new Logic01Soal01(9);
        logic01Soal01.cetakArray();
        System.out.println("\n\nSoal No 2");
        Logic01Soal02 logic01Soal02 = new Logic01Soal02(9);
        logic01Soal02.cetakArray();
        System.out.println("\n\nSoal No 3");
        Logic01Soal03 soal03 = new Logic01Soal03(9);
        soal03.cetakArray();
        System.out.println("\n\nSoal No 4");
        Logic01Soal04 logic01Soal04 = new Logic01Soal04(9);
        logic01Soal04.cetakArray();
        System.out.println("\n\nSoal No 5");
        Logic01Soal05 logic01Soal05 = new Logic01Soal05(9);
        logic01Soal05.cetakArray();
        System.out.println("\n\nSoal No 6");
        Logic01Soal06 logic01Soal06 = new Logic01Soal06(9);
        logic01Soal06.cetakArray();
        System.out.println("\n\nSoal No 7");
        Logic01Soal07 soal07 = new Logic01Soal07(9);
        soal07.cetakArray();
        System.out.println("\n\nSoal No 8");
        Logic01Soal08 soal08 = new Logic01Soal08(9);
        soal08.cetakArray();
        System.out.println("\n\nSoal No 9");
        Logic01Soal09 soal09 = new Logic01Soal09(9);
        soal09.cetakArray();
        System.out.println("\n\nSoal No 10");
        Logic01Soal10 soal10 = new Logic01Soal10(9);
        soal10.cetakArray();

        System.out.println("\n\nLogic02");
        System.out.println("\nSoal No 1");
        Logic02Soal01 logic02Soal01 = new Logic02Soal01(9);
        logic02Soal01.cetakArray();
        System.out.println("\nSoal No 2");
        Logic02Soal02 logic02Soal02 = new Logic02Soal02(9);
        logic02Soal02.cetakArray();
        System.out.println("\nSoal No 3");
        Logic02Soal03 logic02Soal03 = new Logic02Soal03(9);
        logic02Soal03.cetakArray();
        System.out.println("\nSoal No 4");
        Logic02Soal04 logic02Soal04 = new Logic02Soal04(9);
        logic02Soal04.cetakArray();
    }
}
