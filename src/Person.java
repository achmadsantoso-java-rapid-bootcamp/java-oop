public class Person {
    //list property or field
    public Integer id;
    public String name;
    public String gender;
    public String address;
    public int age;

    public void sayHello(){
        System.out.println("Hello nama saya "+name+ " , saya tinggal di "+address);
        System.out.println("Dan saya berumur "+age);
    }

    //list of method getter and setter


    public Person() {
    }

    public Person(Integer id, String name, String gender, String address) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.address = address;
    }

    public Person(Integer id, String name, String gender, String address, int age) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.address = address;
        this.age = age;
    }
}
